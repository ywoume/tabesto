.PHONY: app-install app-up app-up-log app-php app-reload  app-stop
-include .env

SHELL := /bin/bash

# Perl Colors, with fallback if tput command not available
GREEN  := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 2 || echo "")
BLUE   := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 4 || echo "")
WHITE  := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 7 || echo "")
YELLOW := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 3 || echo "")
RESET  := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm sgr0 || echo "")


default: help

DOCKER_SERVICE_PHP ?= php

BLUE := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 4 || echo "")
RESET := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm sgr0 || echo "")

EXTRA_PARAMS ?=
PHP_BIN ?= php
# Add help text after each target name starting with '\#\#'
# A category can be added with @category
HELP_FUN = \
    %help; \
    while(<>) { push @{$$help{$$2 // 'commands'}}, [$$1, $$3] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
    print "usage: make [target]\n\n"; \
    for (sort keys %help) { \
    print "${WHITE}$$_:${RESET}\n"; \
    for (@{$$help{$$_}}) { \
    $$sep = " " x (32 - length $$_->[0]); \
    print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
    }; \
    print "\n"; }


define run-in-container
	@if [ ! -f /.dockerenv -a "$$(docker-compose ps -q $(2) 2>/dev/null)" ]; then \
		docker-compose exec --user $(1) $(2) /bin/sh -c "$(3)"; \
	elif [ $$(env|grep -c "^CI=") -gt 0 -a $$(env|grep -cw "DOCKER_DRIVER") -eq 1 ]; then \
		docker-compose exec --user $(1) -T $(2) /bin/sh -c "$(3)"; \
	else \
		$(3); \
	fi
endef

help: ## Show this help.
	@command -v perl >/dev/null 2>&1 && perl -e '$(HELP_FUN)' $(MAKEFILE_LIST) || echo -e "${YELLOW}make help .${RESET}"

app-install:
	make app-up
	echo "Install composer"
	docker-compose exec php /bin/bash -c "composer selfupdate"
	docker-compose exec php /bin/bash -c "composer install"

app-up:
	@docker-compose up  -d --build --remove-orphans
	@echo "${GREEN}  Tests Tabesto is up ... ${RESET}"

app-up-log:
	@docker-compose up --build --remove-orphans

app-php: ## Go inside php containers
	@docker-compose exec php bash

app-reload: ## reload
	@docker container rm tabesto_php_1 -f
	make app-up

app-stop: # stop docker
	@docker-compose stop

app-cs-check: ## execute PHP CS Fixer in dry mode
	$(call run-in-container,root,$(DOCKER_SERVICE_PHP),$(PHP_BIN) -n vendor/bin/php-cs-fixer fix --allow-risky yes --dry-run --diff --verbose)

app-cs-fix: ## execute PHP CS Fixer
	$(call run-in-container,root,$(DOCKER_SERVICE_PHP),$(PHP_BIN) vendor/bin/php-cs-fixer fix --allow-risky=yes --diff -vvv)
