# Architecture

I've chosen symfony, because first I think that tabesto use symfony technologies.
I have 2-4 hours to build an api so i want to make it quickly.

Why ? 
- Quickly and more maintained for a community of developer
- We can have swagger for api docs.
.....


## 1 - Docker

I use docker env for dev it localy.
I'm php developer, so I have my own docker images. I use ``ywoume/php-8.0`` in docker hub.
because i have more tools inside (autocompletion) .. and with  his compatibility with symfony sf6.

we see ours infrastructure in infra/ folders.
`ìnfra/docker-compose.yml`
to run environement we have thoose command :

````shell
make app-install # for install app
make app-up # for running app
make app-php # to run inside php container
````
we can put variable inside  ``.env`` file
like 

```dotenv
COMPOSE_PROJECT_NAME=tabesto
COMPOSE_FILE=infra/docker-compose.yml

```
## 2 - CI 

For CI i'll use gitlab  ci  ````.gitlab-ci.yml```` because i'll use gitlab for repository.
We  will use  3 step: 
 - build (for building application )
 - tests (run all testing pipline )
 - dependencies (check vulnerability)
 - deploy (if i've time)


