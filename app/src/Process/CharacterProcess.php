<?php


namespace App\Process;


use App\Entity\Character;
use App\Entity\Monster;
use App\Manager\CharacterManager;
use App\Manager\TileManager;
use Doctrine\ORM\EntityManagerInterface;

final class CharacterProcess
{
    public function __construct(
        private CharacterManager $characterManager,
        private MonsterProcess $monsterProcess,
        private TileManager $tileManager,
        private EntityManagerInterface $entityManager
    )
    {
    }

    public function new(): Character
    {
        return $this->characterManager->new();
    }

    public function move(int $id)
    {
        $character = $this->characterManager->findCharacter($id);
        $this->tileManager->findOneByCharacter($character);
        $this->monsterProcess->attack(
            $character,
            $this->tileManager->getCurrentTile()
        );

        $this->tileManager->disable();
        $this->entityManager->flush();
        //new Tile
        $this->tileManager->new();
    }

    public function attack(Monster $monster)
    {

    }

    public function rest()
    {

    }
}
