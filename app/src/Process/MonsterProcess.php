<?php


namespace App\Process;


use App\Entity\Character;
use App\Entity\Monster;
use App\Entity\Tile;
use Doctrine\ORM\EntityManagerInterface;

final class MonsterProcess
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function new(): Monster
    {

    }
    public function attack(Character $character, Tile $tile)
    {
        if ($tile->getMonster()->getPointLife() <= 0) {
            return;
        }
        $monsterAttack = $tile->getMonster()->getPointAttack();
        $personArmor = $character->getArmorValue();
        $personPointLife = $character->getPointLife();
        if ($personArmor > 0 && $monsterAttack  < $personArmor)
        {
            $personArmorRest = $personArmor - $monsterAttack;
            $character->setArmorValue($personArmorRest);
        }else if ($personArmor > 0 && $monsterAttack > $personArmor) {
            $rest = $monsterAttack - $personArmor;
            $lifeRest = $personPointLife - $rest;
            $character->setPointLife($lifeRest);

        }else if ($personArmor < 0 ) {
            $lifeRest = $personPointLife - $monsterAttack;
            $character->setPointLife($lifeRest);
        }

        if ($character->getPointLife() < 0) {
            $this->adventureProcess->end();
        }
        $this->entityManager->flush();
    }
}
