<?php


namespace App\Process;


use App\Entity\Character;
use App\Entity\Monster;
use App\Entity\Tile;
use App\Manager\MonsterManager;
use App\Manager\TileManager;

final class TileProcess
{
    public function __construct(private TileManager $tileManager)
    {
    }

    public function new(): Tile
    {
       $tile = $this->tileManager->new();
       $monster = (new Monster());
       $tile->setMonster($monster);
       return $tile;
    }
}
