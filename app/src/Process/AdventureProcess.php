<?php

namespace App\Process;

use App\Entity\Adventure;
use App\Entity\Character;
use App\Manager\AdventureManager;
use App\Manager\CharacterManager;
use Doctrine\ORM\EntityManagerInterface;

final class AdventureProcess
{
    private \App\Entity\Adventure $adventure;

    public function __construct(
        private AdventureManager $adventureManager,
        private TileProcess $tileProcess,
        private CharacterProcess $characterProcess
    )
    {
    }

    public function start(): void
    {
        $this->adventure = $this->adventureManager->new();
        $person = $this->characterProcess->new();
        $this->tileProcess->new($person);
    }

    public function getAdventure(): Adventure
    {
        return $this->adventure;
    }

    public function boss()
    {

    }


    public function end(): array
    {

    }

    private function getScore():  int
    {

    }



}
