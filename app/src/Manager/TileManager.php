<?php


namespace App\Manager;


use App\Entity\Tile;
use App\Repository\TileRepository;
use Doctrine\ORM\EntityManagerInterface;

final class TileManager
{
    private ?Tile $tile;

    public function __construct(private TileRepository $tileRepository,private EntityManagerInterface $entityManager)
    {
    }

    public function new(): Tile
    {
       return $this->tile = (new Tile())->setEnable(true);
    }


    public function findOneByCharacter(?\App\Entity\Character $character): Tile
    {
        return $this->tile = $this->tileRepository->findOneBy(['person' => $character]);
    }

    public function disable(): void
    {
        $this->tile->setEnable(false);
        $this->save(true);
    }


    public function getCurrentTile(): Tile
    {
        return  $this->tile;
    }

    private function save(bool $isUpdate = false): void
    {
        if (!$isUpdate) {
            $this->entityManager->persist($this->getCurrentTile());
        }
        $this->entityManager->flush();
    }
}
