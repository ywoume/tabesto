<?php

namespace App\Manager;

use App\Entity\Adventure;
use App\Entity\Character;
use App\Entity\Monster;
use App\Entity\Tile;
use App\Repository\AdventureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Log\Logger;

final class AdventureManager
{
    public function __construct(
        private AdventureRepository $adventureRepository,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    )
    {
    }

    public function getAdventure(int $idAdventure)
    {
        $adventure = $this->adventureRepository->find($idAdventure);
        $this->logger->info(sprintf('todo: action of person and his consequences'));
        return [
           'adventure_id' => $adventure?->getId(),
           'score' => $adventure?->getScore(),
           'character_id' => $adventure?->getPerson()?->getId(),
            'tile_id' =>  $adventure?->getTile()?->getId()
        ];
    }

    public function getAdventureTile(int $idAdventure):array
    {
        $adventure = $this->adventureRepository->find($idAdventure);
        return [
            'adventure_id' => $adventure?->getId(),
            'tile_id' =>  $adventure?->getTile()?->getId(),
            'tile.monster_id' =>  $adventure?->getTile()?->getType()
        ];
    }

    public function end(): void
    {

    }

    public function boss()
    {

    }

    public function new(): Adventure
    {
        $adventure =  new Adventure();
        $this->entityManager->persist($adventure);
        $this->entityManager->flush();
        return $adventure;
    }
}
