<?php


namespace App\Manager;

use App\Entity\Character;
use App\Repository\CharacterRepository;
use Doctrine\ORM\EntityManagerInterface;

final class CharacterManager
{
    public function __construct(private CharacterRepository $characterRepository)
    {
    }

    public function findCharacter(int $id): ?Character
    {
       return $this->characterRepository->findOneBy(['id' =>  $id]);
    }

    public function new(): Character
    {
        $character = new Character();
        $this->characterRepository->save($character, true);
        return  $character;
    }
}
