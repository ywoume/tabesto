<?php

/*
 * (c) Tabesto
 */

namespace App\Entity;

use App\Repository\AdventureRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdventureRepository::class)]
final class Adventure
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $score = 0;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Tile $Tile = null;

    #[ORM\OneToOne( cascade: ['persist', 'remove'])]
    private ?Character $person = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getTile(): ?Tile
    {
        return $this->Tile;
    }

    public function setTile(?Tile $Tile): self
    {
        $this->Tile = $Tile;

        return $this;
    }

    public function getPerson(): ?Character
    {
        return $this->person;
    }

    public function setPerson(?Character $person): self
    {
        $this->person = $person;

        return $this;
    }
}
