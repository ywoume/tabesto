<?php

/*
 * (c) Tabesto
 */

namespace App\Entity;

use App\Repository\MonsterRepository;
use App\Type\Monster\Ghost;
use App\Type\Monster\Gobelin;
use App\Type\Monster\Ork;
use App\Type\Monster\Troll;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MonsterRepository::class)]
final class Monster
{
    // use Profile;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING)]
    private string $type;
    private array $allType;

    public function __construct()
    {
        $allType = [
            'ghost' => Ghost::class,
            'gobelin' => Gobelin::class,
            'ork' => Ork::class,
            'troll' => Troll::class,
        ];
        $this->type = array_rand($allType);
        $this->pointAttack = $allType[$this->type]::ATTACK;
        $this->pointLife = $allType[$this->type]::POINT_LIFE;
        $this->armorValue = $allType[$this->type]::ARMOR_VALUE;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    #[ORM\Column]
    private ?int $pointLife = null;

    #[ORM\Column]
    private ?string $pointAttack = null;

    #[ORM\Column]
    private ?int $armorValue = null;

    public function getPointLife(): ?int
    {
        return $this->pointLife;
    }

    public function setPointLife(int $pointLife): self
    {
        $this->pointLife = $pointLife;

        return $this;
    }

    public function getPointAttack(): ?string
    {
        return $this->pointAttack;
    }

    public function setPointAttack(string $pointAttack): self
    {
        $this->pointAttack = $pointAttack;

        return $this;
    }

    public function getArmorValue(): ?int
    {
        return $this->armorValue;
    }

    public function setArmorValue(int $armorValue): self
    {
        $this->armorValue = $armorValue;

        return $this;
    }
}
