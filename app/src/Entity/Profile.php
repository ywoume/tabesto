<?php

/*
 * (c) Tabesto
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Profile
{
    #[ORM\Column]
    private ?int $pointLife = null;

    #[ORM\Column]
    private ?string $pointAttack = null;

    #[ORM\Column]
    private ?int $armorValue = null;

    public function getPointLife(): ?int
    {
        return $this->pointLife;
    }

    public function setPointLife(int $pointLife): self
    {
        $this->pointLife = $pointLife;

        return $this;
    }

    public function getPointAttack(): ?string
    {
        return $this->pointAttack;
    }

    public function setPointAttack(string $pointAttack): self
    {
        $this->pointAttack = $pointAttack;

        return $this;
    }

    public function getArmorValue(): ?int
    {
        return $this->armorValue;
    }

    public function setArmorValue(int $armorValue): self
    {
        $this->armorValue = $armorValue;

        return $this;
    }
}
