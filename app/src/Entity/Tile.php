<?php

/*
 * (c) Tabesto
 */

namespace App\Entity;

use App\Repository\TileRepository;
use App\Type\Tile\Desert;
use App\Type\Tile\Forest;
use App\Type\Tile\GrassLands;
use App\Type\Tile\Hills;
use App\Type\Tile\Montains;
use App\Type\Tile\Swap;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TileRepository::class)]
final class Tile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $type = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $specialEffect = null;

    #[ORM\Column(length: 255, nullable: true)]
    private bool $enable = false;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Monster $monster = null;

    #[ORM\ManyToOne( cascade: ['persist', 'remove'])]
    private ?Character $person = null;

    public function __construct()
    {
        $types = [
            'grasslands' => new GrassLands(),
            'hills' => new Hills(),
            'forest' => new Forest(),
            'mountains' => new Montains(),
            'desert' => new Desert(),
            'swamp' => new Swap(),
        ];
        $this->type = array_rand($types);
        $this->specialEffect = $types[$this->type]->getSpecialEffect($this->monster);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSpecialEffect(): ?string
    {
        return $this->specialEffect;
    }

    public function setSpecialEffect(string $specialEffect): self
    {
        $this->specialEffect = $specialEffect;

        return $this;
    }

    public function getMonster(): ?Monster
    {
        return $this->monster;
    }

    public function setMonster(?Monster $monster): self
    {
        $this->monster = $monster;

        return $this;
    }

    public function isEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(?bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getPerson(): ?Character
    {
        return $this->person;
    }

    public function setPerson(?Character $person): self
    {
        $this->person = $person;

        return $this;
    }
}
