<?php

/*
 * (c) Tabesto
 */

namespace App\Entity;

use App\Repository\CharacterRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CharacterRepository::class)]

final class Character
{
    use Profile;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    public function __construct()
    {
        $this->pointLife = 20;
        $this->pointAttack = '2D6';
        $this->armorValue = 5;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
