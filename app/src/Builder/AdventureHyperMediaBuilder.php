<?php

/*
 * (c) Tabesto
 */

namespace App\Builder;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class AdventureHyperMediaBuilder
{
    public function __construct(
        public ParameterBagInterface $parameterBag
    ) {
    }

    public function buildUrlForAdventure(int $idAdventure): array
    {
        $baseUrl = $this->parameterBag->get('base_url');

        return [
            sprintf('%s/adventure/start', $baseUrl),
            sprintf('%s/adventure/%s', $baseUrl, $idAdventure),
            sprintf('%s/adventure/%s/tile', $baseUrl, $idAdventure),
        ];
    }

    public function buildUrlForCharacter(int $idCharacter): array
    {
        $baseUrl = $this->parameterBag->get('base_url');

        return [
            sprintf('%s/character/%s', $baseUrl, $idCharacter),
            sprintf('%s/character/%s/action/move', $baseUrl, $idCharacter),
            sprintf('%s/character/%s/action/attack', $baseUrl, $idCharacter),
            sprintf('%s/character/%s/action/rest', $baseUrl, $idCharacter),
        ];
    }
}
