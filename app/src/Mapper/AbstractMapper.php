<?php

/*
 * (c) Tabesto
 */

namespace App\Mapper;

abstract class AbstractMapper
{
    protected const TYPE_ADVENTURE = 'Adventure';
    protected const TYPE_CHARACTER = 'Characters';

    abstract public function map(string $event, int $id = null): array;
}
