<?php


namespace App\Mapper;


use App\Builder\AdventureHyperMediaBuilder;
use App\Entity\Character;
use App\Repository\CharacterRepository;
use App\Tests\Unit\Builder\AdventureHyperMediaBuilderTest;

final class CharacterMapper extends AbstractMapper
{
    public function __construct(
        private CharacterRepository $characterRepository,
        private AdventureHyperMediaBuilder $adventureHyperMediaBuilder
    )
    {
    }

    public function map(string $event, int $id = null): array
    {
        $calledMethod = \sprintf('mapFor%s', \ucfirst($event));

        $result = match ($event) {
            'start' => $this->$calledMethod(),
            'detail', 'tile', 'move' => $this->$calledMethod($id),
        };

        $urls = !is_null($id) ? $this->adventureHyperMediaBuilder->buildUrlForCharacter($id): [];
        return [
                '@id' =>  sprintf('Adventure %s', $event),
                'type' => AbstractMapper::TYPE_ADVENTURE,

            ] + $result + ['links' => $urls];

    }


    public function mapForDetail(int $id):array
    {
        $character = $this->characterRepository->findOneBy(['id' =>$id]);
        return [
            'id' => $character->getId(),
            'point_attack' => $character->getPointAttack(),
            'armor_value' => $character->getArmorValue(),
            'point_life' => $character->getPointLife(),
        ];
    }

    public function mapForMove(int $id): array
    {
        return [
            '@id' => 'move-action',
        ];
    }
}
