<?php
namespace App\Mapper;

use App\Builder\AdventureHyperMediaBuilder;
use App\Entity\Adventure;
use App\Manager\AdventureManager;
use App\Process\AdventureProcess;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class AdventureMapper extends AbstractMapper
{
    public const ADVENTURE_START  = 'start';
    public const ADVENTURE_DETAIL  = 'detail';
    public const ADVENTURE_TILE  = 'tile';

    public function __construct(
        private AdventureHyperMediaBuilder $adventureHyperMediaBuilder,
        private AdventureProcess $adventureProcess,
        private AdventureManager $adventureManager
    )
    {}

    public function map(string $event,  int $id = null ): array
    {
        $calledMethod = \sprintf('mapFor%s', \ucfirst($event));

        $result = match ($event) {
            'start' => $this->$calledMethod(),
            'detail', 'tile' => $this->$calledMethod($id),
        };
        $urls = !is_null($id) ? $this->adventureHyperMediaBuilder->buildUrlForAdventure($id): [];
       return [
            '@id' =>  sprintf('Adventure %s', $event),
            'type' => AbstractMapper::TYPE_ADVENTURE,

       ] + $result + ['links' => $urls];
    }


    public function mapForStart():array
    {
        $this->adventureProcess->start();
        return  [
            'id' => $this->adventureProcess?->getAdventure()?->getId(),
            'score' => $this->adventureProcess?->getAdventure()?->getScore(),
        ];
    }

    protected function mapForDetail(int $id): array
    {
        return $this->adventureManager->getAdventure($id);
    }

    protected function mapForTile(int $id):array
    {
        return $this->adventureManager->getAdventureTile($id);
    }
}
