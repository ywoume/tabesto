<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\Ghost;
use App\Type\Monster\MonsterInterface;

final class Hills implements TileTypeInterface
{
    public function getSpecialEffect(?MonsterInterface $monster): ?string
    {
        if ($monster instanceof Ghost) {
            return '+ 2';
        }

        return null;
    }
}
