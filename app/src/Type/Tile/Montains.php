<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\MonsterInterface;
use App\Type\Monster\Troll;

class Montains implements TileTypeInterface
{
    public function __construct()
    {
    }

    public function getSpecialEffect(?MonsterInterface $monster): ?string
    {
        if ($monster instanceof Troll) {
            return '+ 2';
        }

        return null;
    }
}
