<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\MonsterInterface;

interface TileTypeInterface
{
    public function getSpecialEffect(?MonsterInterface $monster): ?string;
}
