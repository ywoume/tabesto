<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\Gobelin;
use App\Type\Monster\MonsterInterface;

class Forest implements TileTypeInterface
{
    public function getSpecialEffect(?MonsterInterface $monster): ?string
    {
        if ($monster instanceof Gobelin) {
            return '+2';
        }

        return null;
    }
}
