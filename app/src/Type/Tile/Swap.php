<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\MonsterInterface;

class Swap implements TileTypeInterface
{
    public function getSpecialEffect(?MonsterInterface $monster): ?string
    {
        return null;
    }
}
