<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\MonsterInterface;

class Desert implements TileTypeInterface
{
    public function getSpecialEffect(?MonsterInterface $monster): ?string
    {
        return null;
    }
}
