<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Tile;

use App\Type\Monster\MonsterInterface;
use App\Type\Monster\Ork;

class GrassLands implements TileTypeInterface
{
    public function getSpecialEffect(?MonsterInterface $monster): ?string
    {
        if ($monster instanceof Ork) {
            return '+ 2';
        }

        return null;
    }
}
