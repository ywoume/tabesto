<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Monster;

use App\Entity\Monster;

final class Ghost implements MonsterInterface // extends Monster
{
    public const POINT_LIFE = 8;

    public const ATTACK = '1D4';

    public const ARMOR_VALUE = 6;
}
