<?php

/*
 * (c) Tabesto
 */

namespace App\Type\Monster;

use App\Entity\Monster;

final class Gobelin implements MonsterInterface // extends Monster
{
    public const POINT_LIFE = 12;

    public const ATTACK = '1D4 - 1';

    public const ARMOR_VALUE = 0;
}
