<?php

/*
 * (c) Tabesto
 */

namespace App\Controller;

use App\Mapper\AdventureMapper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/adventure', 'api_adventure_')]
final class AdventureController extends AbstractController // implements ApiAdventure
{
    #[Route('/start', name: 'start', methods: 'POST')]
    public function startAdventure(AdventureMapper $adventureMapper): JsonResponse
    {
        return $this->json(
            $adventureMapper->map(AdventureMapper::ADVENTURE_START)
        );
    }

    #[Route("/{idAdventure}", name: 'get_adventure',  methods:'GET')]
    public function getAdventure(string $idAdventure, AdventureMapper $adventureMapper): JsonResponse
    {
        return $this->json(
            $adventureMapper->map('detail',$idAdventure)
        );
    }

    #[Route('/{idAdventure}/tile',name: 'get_title', methods: 'GET')]
    public function getAdventureTile(int $idAdventure,AdventureMapper $adventureMapper):JsonResponse
    {
        return   $this->json($adventureMapper->map('tile', $idAdventure));
    }
}
