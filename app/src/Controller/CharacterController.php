<?php


namespace App\Controller;


use App\Mapper\CharacterMapper;
use App\Repository\CharacterRepository;
use App\Repository\TileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/character','api_character_')]
final class CharacterController extends AbstractController #implements ApiCharacter
{
    #[Route(path:  '/{idCharacter}',name: '_get_id', methods: 'GET', stateless: true)]
    public function getCharacter(int $idCharacter, CharacterMapper $mapper): JsonResponse
    {
        return $this->json($mapper->map('detail',$idCharacter));
    }

    #[Route(path:  '/{idCharacter}/action/move',name: '_action_move', methods: 'POST', stateless: true)]
    public function moveCharacter(int $idCharacter, CharacterMapper $characterMapper): JsonResponse
    {
        return  $this->json(['msg' => 'move']);
    }

    public function restCharacter(int $idCharacter): JsonResponse
    {
        return  $this->json(['msg' => 'rest']);
    }
}
