<?php

/*
 * (c) Tabesto
 */

namespace App\Tests\Unit\Builder;

use App\Builder\AdventureHyperMediaBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class AdventureHyperMediaBuilderTest extends TestCase
{
    protected function setUp(): void
    {
        $this->mockParameterBag = $this->createMock(ParameterBagInterface::class);
        $this->class = new AdventureHyperMediaBuilder($this->mockParameterBag);
    }

    /**
     * @dataProvider provide
     */
    public function testBuildUrlForAdventure(array $adventure, array $character): void
    {
        $this->mockParameterBag->method('get')->willReturn('http://baseurl');
        $resultAdventure = $this->class->buildUrlForAdventure(1);
        $this->assertIsArray($resultAdventure);
        $this->assertSame($resultAdventure, $adventure);
        $resultCharacter = $this->class->buildUrlForCharacter(1);
        $this->assertIsArray($resultCharacter);
        $this->assertSame($resultCharacter, $character);
    }

    public function provide(): \Generator
    {
        yield 'adventure' => [
            [
                'http://baseurl/adventure/start',
                'http://baseurl/adventure/1',
                'http://baseurl/adventure/1/tile',
            ],
            [
                'http://baseurl/character/1',
                'http://baseurl/character/1/action/move',
                'http://baseurl/character/1/action/attack',
                'http://baseurl/character/1/action/rest',
            ],
        ];
    }
}
