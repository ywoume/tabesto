<?php
namespace App\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

final class ProcessTest extends WebTestCase
{
    protected function setUp(): void
    {
        $cmd  = Process::fromShellCommandline('php bin/console d:d:c');
        $cmd->run();

        $cmd  = Process::fromShellCommandline('php bin/console d:s:u -f');
        $cmd->run();

    }

    public function testStart()
    {
        $client = static::createClient();
        $client->request('POST', '/adventure/start');
        $this->assertResponseIsSuccessful();
    }
}
