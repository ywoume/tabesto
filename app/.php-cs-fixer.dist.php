<?php

$finder = (new PhpCsFixer\Finder())
    ->in(['config', 'public', 'src', 'tests'])
    ->exclude('var','vendor')
;
$header=<<<COMMENT
(c) Tabesto
COMMENT;
return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'array_indentation' => true,
        'array_syntax' => ['syntax' => 'short'],
        'blank_line_after_namespace' => true,
        'header_comment' => ['header' => $header],
        'indentation_type' => true,
        'linebreak_after_opening_tag' => true,
        'constant_case' => true,
        'lowercase_keywords' => true,
        'class_attributes_separation' => ['elements' => ['method' => 'one']],
        'no_extra_blank_lines' => [
            'tokens' => [
                'break',
                'continue',
                'curly_brace_block',
                'extra',
                'parenthesis_brace_block',
                'return',
                'square_brace_block',
                'throw',
                'use',
            ],
        ],
        'no_useless_else' => true,
        'no_useless_return' => true,
        'ordered_imports' => true,
        'phpdoc_add_missing_param_annotation' => true,
        'phpdoc_order' => true,
        'semicolon_after_instruction' => true,
        'visibility_required' => true,
    ])
    ->setFinder($finder)
;
